--
-- File generated with SQLiteStudio v3.4.4 on ส. ต.ค. 7 14:32:58 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE IF NOT EXISTS category (
    category_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    category_name TEXT (50) NOT NULL
);

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         1,
                         'MyCoffee'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         2,
                         'dessert'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         3,
                         'candy'
                     );


-- Table: customer
DROP TABLE IF EXISTS customer;

CREATE TABLE IF NOT EXISTS customer (
    customer_id   INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    customer_name TEXT,
    customer_tel  TEXT    UNIQUE
);

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel
                     )
                     VALUES (
                         1,
                         'menoi',
                         '0885666667'
                     );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE IF NOT EXISTS product (
    product_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name        TEXT (50) UNIQUE,
    product_price       DOUBLE    NOT NULL,
    product_size        TEXT (5)  DEFAULT SML
                                  NOT NULL,
    product_sweet_level TEXT (5)  DEFAULT (123) 
                                  NOT NULL,
    product_type        TEXT (5)  DEFAULT HCF
                                  NOT NULL,
    category_id         INTEGER   DEFAULT (1) 
                                  NOT NULL
                                  REFERENCES category (category_id) ON DELETE SET NULL
                                                                    ON UPDATE CASCADE
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        1,
                        'Espresso',
                        30.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        2,
                        'Americano',
                        40.0,
                        'SML',
                        '012',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        3,
                        'เค้กชิฟฟ่อนช็อกโกแลต',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        4,
                        'บัตเตอร์เค้ก',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );


-- Table: reciept
DROP TABLE IF EXISTS reciept;

CREATE TABLE IF NOT EXISTS reciept (
    reciept_id   INTEGER  PRIMARY KEY ASC AUTOINCREMENT,
    reciept_date DATETIME DEFAULT (CURRENT_TIMESTAMP) 
                          NOT NULL,
    total        NUMERIC,
    cash         REAL,
    total_qty    INTEGER,
    user_id      INTEGER  REFERENCES user (user_id) ON DELETE RESTRICT
                                                    ON UPDATE CASCADE
                          NOT NULL,
    customer_id  INTEGER  REFERENCES customer (customer_id) ON DELETE RESTRICT
                                                            ON UPDATE CASCADE
);

INSERT INTO reciept (
                        reciept_id,
                        reciept_date,
                        total,
                        cash,
                        total_qty,
                        user_id,
                        customer_id
                    )
                    VALUES (
                        1,
                        '2023-10-07 07:01:32',
                        160,
                        1000.0,
                        4,
                        6,
                        1
                    );


-- Table: reciept_detail
DROP TABLE IF EXISTS reciept_detail;

CREATE TABLE IF NOT EXISTS reciept_detail (
    reciept_detail_id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    product_id        INTEGER REFERENCES product (product_id) ON DELETE RESTRICT
                                                              ON UPDATE CASCADE,
    product_name      TEXT,
    product_price     REAL,
    qty               INTEGER,
    total_price       REAL,
    reciept_id        INTEGER REFERENCES reciept (reciept_id) ON DELETE CASCADE
                                                              ON UPDATE CASCADE
);

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               1,
                               1,
                               'Espresso',
                               30.0,
                               2,
                               60.0,
                               1
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               2,
                               3,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               1
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               3,
                               2,
                               'Americano',
                               40.0,
                               1,
                               40.0,
                               1
                           );


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE IF NOT EXISTS user (
    user_id       INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    user_login    TEXT (50) UNIQUE,
    user_gender   TEXT (3)  NOT NULL,
    user_password TEXT (50) NOT NULL,
    user_role     INTEGER   NOT NULL,
    user_name     TEXT (50) 
);

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     6,
                     'werapan',
                     'M',
                     'password',
                     1,
                     'Worawit'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     10,
                     'admin',
                     'M',
                     'password',
                     0,
                     'Administrator'
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
